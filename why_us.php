<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Fwg </title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <script src="https://npmcdn.com/flickity@2/dist/flickity.pkgd.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="css/service3.css">
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

      <style>
      #mySidenav a {
  position: fixed;
  left: -80px;
  transition: 0.3s;
  padding: 15px;
  width: 100px;
  text-decoration: none;
  font-size: 20px;
  margin-top:210px;
  color: white;
  border-radius: 0 5px 5px 0;
}

#mySidenav a:hover {
  left: 0;
}

#about1 {
  top: 20px;
  background-color: blue;
}

#blog {
  top: 80px;
  background-color:#f44336;
}

#projects {
  top: 140px;
  background-color: skyblue;
}

#contact1 {
  top: 200px;
  background-color: #555
}


</style>
   </head>
   <body>
        <div id="mySidenav" class="sidenav">
                <a href="https://www.facebook.com/fwgind/" id="about1">FB</a>
                <a href="callto:8861715281" id="blog">Call Me</a>
                <a href="emailto:info@fwg.my" id="projects">Email</a>
                <a href="emailto:info@fwg.my" id="contact1">Feedback</a>
              </div>
             
      <div class="cursor"></div>
  <div class="brandname">
      <a href="index.php"> <img src="images/FWGIndialogo.jpg" class="logoid"></a>
         <h1>
FreightWatch G Security Services India Pvt Ltd
                </h1>
                <h4>Securing Global Trade</h4>
                </div>
              <section id="header">
            
                 <div class="menu-bar">
                
                 <nav class="navbar navbar-expand-lg navbar-light hide" id="hide"> 
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNav">
                         <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div>
                 </nav></section>
                 
                 <section id="header1">
            
                    <div class="menu-bar1">
                   
                    <nav class="navbar1 navbar-expand-lg navbar-light">
                       <a class="navbar-brand" href="#"></a>
                       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                       <span class="navbar-toggler-icon"></span>
                       </button>
                       <div class="container">
                       <div class="collapse navbar-collapse" id="navbarNav">
                                          <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div></nav>                      
      </section>
    
             <!-- Start video hero -->
       <div class="video-hero jquery-background-video-wrapper demo-video-wrapper">
         <video class="jquery-background-video" autoplay muted loop poster="">
           <source src="Why us.mp4" type="video/mp4">
         </video>
            <div class="video-overlay"></div>
            <div class="page-width">
                 <div class="video-hero--content">
                      <h1>Fwg India</h1>
                      <p>India is Safe Now</p>
                 </div>
            </div>
       </div>
       <!-- End video hero -->
      </div>
      <div class="services"id="id1">
    <h1 style="text-align: center;font-weight:800;margin:28px 0px;">
        Services</h1>
    <div class="container">
       <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bold;    letter-spacing: 1px;
">More than two Decade of Experience and Stability.</h4></a>
                </div>
                <!---<div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
             <li style="font-size: 18px;font-weight:bold;"> Static Guarding (Unarmed)</li>

<li style="font-size: 18px;font-weight:bold;">Mobile Patrol</li>

<li style="font-size: 18px;font-weight:bold;">Escorting & Personal Protection</li>

<li style="font-size: 18px;font-weight:bold;">Event Security</li>

<li style="font-size: 18px;font-weight:bold;">Canine Units</li>
</ul>
                </div>--->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;   letter-spacing: 0.2px;
    text-align: justify;
">Quality and Reliable Services according to Customers'needs.</h4>
                </div>
                <!---<div class="card-body dogservice_content">
                      <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</li></ul>
                </div>---->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;letter-spacing: 0.2px;
    text-align: justify;">Listen to the needs of the Customer and Design a Security Program tailored to them
at a fair Price.</h4>
                </div>
               <!--- <div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Security Audit</li>

<li style="font-size: 18px;font-weight:bold;"> Security Risk Management</li>

<li style="font-size: 18px;font-weight:bold;">Emergency/ Crisis Management </li>

<li style="font-size: 18px;font-weight:bold;">Business Continuity Management</li>
<br>
</ul>
                    
                </div>--->
             </div>
          </div>
       </div>
       <br>
         <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bold;text-align:justify;letter-spacing: 1px;">Provide Customer with the Complete Security Package.</h4>
                </div>
              <!----  <div class="card-body dogservice_content">
                    <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</ul>
</div>      
          --->   </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;text-align:justify;letter-spacing: 1px;">Put our Customers before ourselves and do our Best to support their Goals.</h4>
                </div>
               <!--- <div class="card-body">
                <p style="font-size:18px;font-weight:bold;">Drug Tracking Survivor Rescue Mission (earthquakes, building collapse and etc) Escort Services – Cargo and VIP Cash In Transit
                    – where applicable Crowd Control – Functions / Games / Concerts Bomb Squad</p>
                   
                </div>-->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card" data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;text-align:justify;">Integrated with Technology to provide Value-Added Services through E-Channels to Clients.</h4>
                </div>
                <!---<div class="card-body">
                    <p style="font-size:18px;font-weight:bold;    margin-bottom: 32px;
">OUR VISITOR MANAGEMENT SYSTEM SETUP (VMS) An immense value addition visitor service Easy 
                   retrieval of  <div id="myDIV1" style="display: none">  photographs                  
and details across departments and floors.                                        
 Ease of installation and use. Zero maintenance. Temporary Visitor
pa                    
sses for any period of time for regular visitors such as Courier boys, canteen persons etc.,
Reports date wise, visitor wise, depart                    
ent wise, floor wise, employee wise and simple query based reports. WHY VMS The objective of VMS is to monitor,manage and audit
visitor traffic using customized VMS software as a standalone system. VMS lets you define user security levels, designated access areas, badge visito                    
s and then track those visitors. With VMS, the proposed system records everyone and vehicle that enters and exits the building. Benef                    
its 1) Photographic records of visitors 2) Restricted access to visitors to any particular department or area 3) Monitoring the visitors & their activities in the                    
company 4) Prescheduled appointments by officers can be stored and make the visitor pass Issuing easy. 5) Software can be integrated with existing security / access control systems,                         
               Such as Barcode system, Smart Card, Stripe card based time & attendance system etc</div>
                     <button onclick="myFunction1()" class="btn btn-danger" style="width:99px;"> View-Details</button>
                </button>
                   </p>
                </div>--->
             </div>
          </div>
       </div>
    </div></br>
 <div class="container">
       <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bold;padding-top:9px;text-align:justify;letter-spacing:0.5px;">Statutory Compliant Company.</h4></a>
                </div>
                <!---<div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
             <li style="font-size: 18px;font-weight:bold;"> Static Guarding (Unarmed)</li>

<li style="font-size: 18px;font-weight:bold;">Mobile Patrol</li>

<li style="font-size: 18px;font-weight:bold;">Escorting & Personal Protection</li>

<li style="font-size: 18px;font-weight:bold;">Event Security</li>

<li style="font-size: 18px;font-weight:bold;">Canine Units</li>
</ul>
                </div>--->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;padding-top:9px;text-align:justify;letter-spacing: 1px;">In-house Training Centre.</h4>
                </div>
               <!--- <div class="card-body dogservice_content">
                      <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</li></ul>
                </div>-->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;padding-top:9px;text-align:justify;">Contract Start Up Team.
</h4>
                </div>
               <!--- <div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Security Audit</li>

<li style="font-size: 18px;font-weight:bold;"> Security Risk Management</li>

<li style="font-size: 18px;font-weight:bold;">Emergency/ Crisis Management </li>

<li style="font-size: 18px;font-weight:bold;">Business Continuity Management</li>
<br>
</ul>
                    
                </div>--->
             </div>
          </div>
       </div>
       <br>
         <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bold;text-align:justify;letter-spacing: 1px;">Assignment Site and Post Instructions.</h4>
                </div>
                <!---<div class="card-body dogservice_content">
                    <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</ul>
</div> --->     
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;text-align:justify;">Operation Executives / Night Patrol fficers with Company Vehicles. .</h4>
                </div>
               <!--- <div class="card-body">
                <p style="font-size:18px;font-weight:bold;">Drug Tracking Survivor Rescue Mission (earthquakes, building collapse and etc) Escort Services – Cargo and VIP Cash In Transit
                    – where applicable Crowd Control – Functions / Games / Concerts Bomb Squad</p>
                   
                </div>-->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card" data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;text-align:justify;">24x7 In-House Control Room Monitoring System.</h4></h4>
                </div>
               <!--- <div class="card-body">
                    <p style="font-size:18px;font-weight:bold;    margin-bottom: 32px;
">OUR VISITOR MANAGEMENT SYSTEM SETUP (VMS) An immense value addition visitor service Easy 
                   retrieval of  <div id="myDIV1" style="display: none">  photographs                  
and details across departments and floors.                                        
 Ease of installation and use. Zero maintenance. Temporary Visitor
pa                    
sses for any period of time for regular visitors such as Courier boys, canteen persons etc.,
Reports date wise, visitor wise, depart                    
ent wise, floor wise, employee wise and simple query based reports. WHY VMS The objective of VMS is to monitor,manage and audit
visitor traffic using customized VMS software as a standalone system. VMS lets you define user security levels, designated access areas, badge visito                    
s and then track those visitors. With VMS, the proposed system records everyone and vehicle that enters and exits the building. Benef                    
its 1) Photographic records of visitors 2) Restricted access to visitors to any particular department or area 3) Monitoring the visitors & their activities in the                    
company 4) Prescheduled appointments by officers can be stored and make the visitor pass Issuing easy. 5) Software can be integrated with existing security / access control systems,                         
               Such as Barcode system, Smart Card, Stripe card based time & attendance system etc</div>
                     <button onclick="myFunction1()" class="btn btn-danger" style="width:99px;"> View-Details</button>
                </button>
                   </p>
                </div>--->
             </div>
          </div>
       </div>
    </div></br>
     <div class="container">
       <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bold;padding-top:9px;text-align:justify;">Regular Client Meetings.</h4></a>
                </div>
                <!---<div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
             <li style="font-size: 18px;font-weight:bold;"> Static Guarding (Unarmed)</li>

<li style="font-size: 18px;font-weight:bold;">Mobile Patrol</li>

<li style="font-size: 18px;font-weight:bold;">Escorting & Personal Protection</li>

<li style="font-size: 18px;font-weight:bold;">Event Security</li>

<li style="font-size: 18px;font-weight:bold;">Canine Units</li>
</ul>
                </div>-->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;padding-top:9px;text-align:justify;">Emergency Response Services.
</h4>
                </div>
                <!---<div class="card-body dogservice_content">
                      <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</li></ul>
                </div>--->
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:navy;font-size: 24px;font-weight: bolder;text-align:justify;letter-spacing: 0.6px;
">Provision of Security Solutions via Integrated Security Manpower and Systems.</h4>
                </div>
               <!--- <div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Security Audit</li>

<li style="font-size: 18px;font-weight:bold;"> Security Risk Management</li>

<li style="font-size: 18px;font-weight:bold;">Emergency/ Crisis Management </li>

<li style="font-size: 18px;font-weight:bold;">Business Continuity Management</li>
<br>
</ul>
                    
                </div>-->
             </div>
          </div>
       </div></div></div>
       <br></div>
            <div class="foot">
         <div class="container">
            <img src="images\FWG_web_Footer.png" style="padding-top: 05%;width:40%">
            <h2>Freightwatch G Security Services India Private Limited</h2>
         </div>
      </div>
      <div class="footer">
         Developed by <font color="#59FF00">Anlyz</font><font color="#0039BF">360</font>
      </div>
      <script type="text/javascript" src="js/jquery.flexisel.js"></script>
      <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d5bcd5477aa790be32fce15/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
      <!-- //flexisel -->
      <!-- gallery-pop-up -->
      <script src="js/lsb.min.js"></script>
      <script>
         $(window).load(function() {
         	  $.fn.lightspeedBox();
         	});
      </script>
      <!-- //gallery-pop-up -->
      <!-- flexSlider -->
      <script defer src="js/jquery.flexslider.js"></script>
      <script type="text/javascript">
         $(window).load(function(){
           $('.flexslider').flexslider({
         	animation: "slide",
         	start: function(slider){
         	  $('body').removeClass('loading');
         	}
           });
         });
      </script>
      <!-- //flexSlider -->
      <!-- start-smooth-scrolling -->
      <script type="text/javascript" src="js/move-top.js"></script>
      <script type="text/javascript" src="js/easing.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
         	$(".scroll").click(function(event){		
         		event.preventDefault();
         		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
         	});
         });
      </script>
            <script>
              window.addEventListener('scroll', scrollEvent)
                var hide =document.getElementById('hide')
                console.log(hide.classList)

                function scrollEvent(){
                  if(window.pageYOffset > 300)
                {
                  hide.classList.remove('hide') 
                } else if(window.pageYOffset < 300){
                  hide.classList.add('hide') 

                }
                }



                </script>
                 <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
                 <script>
                   AOS.init();
                 </script>
                <script>
                <script>
                
$('.jquery-background-video').bgVideo({fadeIn: 2000});
                </script>
   </body>
</html>