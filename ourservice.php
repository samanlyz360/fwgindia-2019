<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Fwg </title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <script src="https://npmcdn.com/flickity@2/dist/flickity.pkgd.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
      <!---card--
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
      <link rel="stylesheet" href="cards-gallery.css">
      <!---cards--end-->
      <link rel="stylesheet" href="ostyle.css">
      
      <style>
      #mySidenav a {
  position: fixed;
  left: -80px;
  transition: 0.3s;
  padding: 15px;
  width: 100px;
  text-decoration: none;
  font-size: 20px;
  margin-top:210px;
  color: white;
  border-radius: 0 5px 5px 0;
}

#mySidenav a:hover {
  left: 0;
}

#about1 {
  top: 20px;
  background-color: blue;
}

#blog {
  top: 80px;
  background-color:#f44336;
}

#projects {
  top: 140px;
  background-color: skyblue;
}

#contact1 {
  top: 200px;
  background-color: #555
}
   #myDIV1 {
    width: 100%;
    /* padding: 50px 0; */
    /* text-align: center; */
    /* background-color: lightblue; */
    margin-top: -17px;
}
.card-img {
    width: 58%;
    border-radius: calc(.25rem - 1px);
}
</style>
   </head>
   <body>
       <div id="mySidenav" class="sidenav">
                <a href="https://www.facebook.com/fwgind/" id="about1">FB</a>
                <a href="callto:8861715281" id="blog">Call Me</a>
                <a href="gmailto:info@fwg.my" id="projects">Email</a>
                <a href="gmailto:info@fwg.my" id="contact1">Feedback</a>
              </div>
             
      <div class="cursor"></div>
 <div class="brandname">
                <a href="index.php"> <img src="images/FWGIndialogo.jpg" class="logoid"></a>
         <h1>
FreightWatch G Security Services India Pvt Ltd
                </h1>
                <h4>Securing Global Trade</h4>
                </div>
              <section id="header">
            
                 <div class="menu-bar">
                
                 <nav class="navbar navbar-expand-lg navbar-light hide" id="hide"> 
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNav">
                         <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div>
                 </nav></section>
                 
                 <section id="header1">
            
                    <div class="menu-bar1">
                   
                    <nav class="navbar1 navbar-expand-lg navbar-light">
                       <a class="navbar-brand" href="#"></a>
                       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                       <span class="navbar-toggler-icon"></span>
                       </button>
                       <div class="container">
                       <div class="collapse navbar-collapse" id="navbarNav">
                                          <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div></nav>                      
      </section>
    
               <!-- Start video hero -->
       <div class="video-hero jquery-background-video-wrapper demo-video-wrapper">
         <video class="jquery-background-video" autoplay muted loop poster="">
           <source src="New video.mp4" type="video/mp4">
         </video>
            <div class="video-overlay"></div>
            <div class="page-width">
                 <div class="video-hero--content">
                      <h1>Fwg India</h1>
                      <p>India is Safe Now</p>
                 </div>
            </div>
       </div>
       <!-- End video hero -->
<br><br>
<!-----services--
<div class="services"id="id1">
    <h1 style="text-align: center;font-weight:800;margin:28px 0px;">
        Services</h1>
    <div class="container">
       <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bold;">Manned Security Services</h4></a>
                </div>
                <div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
             <li style="font-size: 18px;font-weight:bold;"> Static Guarding (Unarmed)</li>

<li style="font-size: 18px;font-weight:bold;">Mobile Patrol</li>

<li style="font-size: 18px;font-weight:bold;">Escorting & Personal Protection</li>

<li style="font-size: 18px;font-weight:bold;">Event Security</li>

<li style="font-size: 18px;font-weight:bold;">Canine Units</li>
</ul>
                </div>
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bolder;">Security Technology Services</h4>
                </div>
                <div class="card-body dogservice_content">
                      <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</li></ul>
                </div>
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bolder;">Security Ailled Consultancy</h4>
                </div>
                <div class="card-body dogservice_content">
                     <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Security Audit</li>

<li style="font-size: 18px;font-weight:bold;"> Security Risk Management</li>

<li style="font-size: 18px;font-weight:bold;">Emergency/ Crisis Management </li>

<li style="font-size: 18px;font-weight:bold;">Business Continuity Management</li>
<br>
</ul>
                    
                </div>
             </div>
          </div>
       </div>
       <br>
         <div class="row">
          <div class="col-sm-4">
             <div class="card"data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-cog fa-spin" style="font-size:54px;color: tomato;"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bold;">Security Training</h4>
                </div>
                <div class="card-body dogservice_content">
                    <ul style="list-style-type:none;">
                 <li style="font-size: 18px;font-weight:bold;"> Alarm Monitoring Service (CMS)</li>

<li style="font-size: 18px;font-weight:bold;">Security Systems & Electronic/

Technology</li>

<li style="font-size: 18px;font-weight:bold;">Visitor Management System (VMS)</li>

<li style="font-size: 18px;font-weight:bold;">Alarm Response</li>
</ul>
</div>      
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card"data-aos="flip-right">
                <div class="card-header">
                   <i class="fa fa-cogs fa-spin" style="font-size:54px;color:tomato"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bolder;">Dog Services</h4>
                </div>
                <div class="card-body">
                <p style="font-size:18px;font-weight:bold;">Drug Tracking Survivor Rescue Mission (earthquakes, building collapse and etc) Escort Services – Cargo and VIP Cash In Transit
                    – where applicable Crowd Control – Functions / Games / Concerts Bomb Squad</p>
                   
                </div>
             </div>
          </div>
          <div class="col-sm-4">
             <div class="card" data-aos="flip-left">
                <div class="card-header">
                   <i class="fa fa-wrench fa-spin" style="font-size:48px;color:tomato"></i><br><br>
                   <h4 style="color:orangered;font-size: 24px;font-weight: bolder;">Systems & Solutions – VMS</h4>
                </div>
                <div class="card-body">
                    <p style="font-size:18px;font-weight:bold;    margin-bottom: 32px;
">OUR VISITOR MANAGEMENT SYSTEM SETUP (VMS) An immense value addition visitor service Easy 
                   retrieval of  <div id="myDIV1" style="display: none">  photographs                  
and details across departments and floors.                                        
 Ease of installation and use. Zero maintenance. Temporary Visitor
pa                    
sses for any period of time for regular visitors such as Courier boys, canteen persons etc.,
Reports date wise, visitor wise, depart                    
ent wise, floor wise, employee wise and simple query based reports. WHY VMS The objective of VMS is to monitor,manage and audit
visitor traffic using customized VMS software as a standalone system. VMS lets you define user security levels, designated access areas, badge visito                    
s and then track those visitors. With VMS, the proposed system records everyone and vehicle that enters and exits the building. Benef                    
its 1) Photographic records of visitors 2) Restricted access to visitors to any particular department or area 3) Monitoring the visitors & their activities in the                    
company 4) Prescheduled appointments by officers can be stored and make the visitor pass Issuing easy. 5) Software can be integrated with existing security / access control systems,                         
               Such as Barcode system, Smart Card, Stripe card based time & attendance system etc</div>
                     <button onclick="myFunction1()" class="btn btn-danger" style="width:99px;"> View-Details</button>
                </button>
                   </p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 -->
 <section class="gallery-block cards-gallery">
	    <div class="container">
	        <div class="heading">
	          <h2>Services</h2>
	        </div>
	        <div class="row">
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
	                	<div class="card-header">Manned Services</div>
	                		<a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                    <div class="card-body card-body1">
	                        
	                       <ul>
	                          <li>Static Guarding (Unarmed)</li>
                               <li>Mobile Patrol</li>
                               <li>Escorting & Personal Protection</li>
                               <li>Event Security</li>
                               <li>Canine Units</li>

	                       </ul>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
	                    	<div class="card-header">SECURITY ALLIED CONSULTANCY</div>
	                    	<a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                    <div class="card-body card-body1">
	                        
	                       <ul>
	                          <li>Security Audit</li>
                               <li>Security Risk Management</li>
                               <li>Emergency/ Crisis Management</li>
                               <li>Business Continuity Management</li>

	                       </ul>
	                    </div>
	                
	                </div>
	            </div>
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
	                	 
	                   	<div class="card-header">FACILITY MANAGEMENT</div>
	                   	<a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                		                    <div class="card-body card-body1">
	                       <ul>
	                          <li>House Keeping</li><br><br>
                               <li>Office boy</li><br><br>
                               <li style="float:left;">Cleaning and Services</li><br><br>
                               <li>Alarm Response</li>
	                       </ul>
	                    </div>

	                </div>
	            </div>
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
	                
	                   	<div class="card-header">SECURITY TECHNOLOGY </div>
	                   	 <a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                    <div class="card-body2 card-body1">
	                       <ul>
	                          <li>Alarm Monitoring Service (CMS)</li><br>
                               <li>Security Systems & </li>
                               <li>Electronic/Technology</li><br>
                               <li>Visitor Management System (VMS)</li>
                               <li>Alarm Response</li><br>
                               <li>khjnihg</li>
                               <li></li>
	                       </ul>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
                    	<div class="card-header">SECURITY TRAINING</div>
                    	 <a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                    <div class="card-body card-body1">
	                       <ul>
	                          <li>Crime Prevention </li><li>and Anti-Terrorism</li>
                               <li>Asset Protection - Loss Prevention</li>
                               <li>Visitor Management System (VMS)</li>
                               <li>Physical Security Management</li>
                               <li>Crisis Management</li>
                               <li>Emergency & Bomb Threat</li>

	                       </ul>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6 col-lg-4 col-sm-4">
	                <div class="card border-0 transform-on-hover">
	                 <div class="card-header">FWG SYSTEMS & SOLUTIONS</div>
	                    <a class="lightbox" href="../img/image4.jpg">
	                	</a>
	                    <div class="card-body2 card-body1">
	                       <ul>
	                          <li>An immense value addition visitor</li>
                               <li>Service Easy<li>-<li>retrieval of photographs</li>
                               <li>Details across departments </li>
                               <li>Details across floors</li>

	                       </ul>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.cards-gallery', { animation: 'slideIn'});
    </script>
<div class="serv">
<h2 style="text-align: center;font-weight:900;color:black;">Brands We Work</h2>
<br>
<div class="row row23">
<div class="col-sm-3"data-aos="flip-left">
<div class="card"data-aos="flip-left">
<img src="images/old/toyoto.png"class="card-img" style="width:59%" >
</div></div>
<div class="col-sm-3">
<div class="card"data-aos="flip-left">
    <img src="images/old/supreme computers.png"style="
    height: 120px;
    width: 233px;
    margin-left: 26px;
"class="card-img" >

</div></div>
<div class="col-sm-3">
<div class="card"data-aos="flip-left">
    <img src="images/old/cauvery News.jpg"style="
    height: 120px;
    width: 233px;
    margin-left: 26px;"class="card-img" >

</div></div>
<div class="col-sm-3">
<div class="card"data-aos="flip-left">
    <img src="images/old/fast logistics.jpg"style="
    height: 120px;
    width: 233px;
    margin-left: 26px;"class="card-img" >

</div>
</div> </div></div>
<div class="row row231"style="margin-left:30px;">
  <div class="col-3">
    <div class="card"data-aos="flip-left">
<img src="images/old/eversendai.jpg"style="
    height: 120px;
    width: 233px;
    margin-left: 26px;"class="card-img" >
</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/nestle.png"class="card-img"style="width: 40%;
    margin-left: 78px;"
 >

</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/moto.png"class="card-img"style="width:59%" >

</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/flex.png"class="card-img"style="width: 98%;
    margin-left: 0px;" >

</div>

  </div> 
  <div id="myDIV1" style="display: none">
  <div class="row row231"style="margin-left:30px;">
  <div class="col-3">
    <div class="card"data-aos="flip-left">
<img src="images/old/eversendai.jpg"style="
    height: 120px;
    width: 233px;
    margin-left: 26px;"class="card-img" >
</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/nestle.png"class="card-img"style="width: 40%;
    margin-left: 78px;"
 >

</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/moto.png"class="card-img"style="width:59%" >

</div></div>
<div class="col-3">
    <div class="card"data-aos="flip-left">
        <img src="images/old/flex.png"class="card-img"style="width: 98%;
    margin-left: 0px;" >

</div>

  </div></div>
   <br><br>
   <button onclick="myFunction1()" class="btn btn-danger btns" style="width:99px;"> View-Details</button>
                </button>
 </div>
</div>
<div class="testimonial3">
<!-- Photo Grid -->

<div class="benifits">
<div class="benifit-content">
  <br>
<h1 style="color:white; text-align:center;">SERVICES BENIFITS</h1>
</div><br><br>
<div class="container">
<div class="row">

  <div class="col-sm-4 col-b"data-aos="flip-left">
<h4 style="text-align: center;font-weight:  bold;">Technical Security Surveys</h4>
</div>
<div class="col-sm-4 col-b col-bb"data-aos="flip-right" >
  <h4 style="text-align: center;font-weight:  bold;" ></h4>
</div>
<div class="col-sm-4 col-b"data-aos="flip-left">
  <h4 style="text-align: center;font-weight:  bold;">CCTV Network system</h4>
</div>
</div><br><br>
<div class="row">

<div class="col-sm-4 col-b1"data-aos="flip-right">
<h4 style="text-align: center;font-weight:  bold;">Mobile Petrol Management</h4>
</div>
<div class="col-sm-4 col-b col-bb"data-aos="flip-left">
<h4 style="text-align: center;font-weight:  bold;color:white"> Technical Security Surveys</h4>
</div>
<div class="col-sm-4 col-b"data-aos="flip-left">
<h4 style="text-align: center;font-weight:  bold;">Access Control System</h4>
</div>
</div>
</div></div>
              <section class="testimonials text-center">
        <div class="container">
    
            <div class="heading white-heading">
                Testimonial
            </div>
            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
             
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">
                            <img src="images/sp.jpg" class="img-circle img-responsive" />
<p>Best Security Service Providers i have ever seen. Very sincere,loyal and honesty in dedicating their services in a right way. Providing complete satisfaction and worth cost
effective to find them in Chennai.
</p>                            <h4>Suresh SPNETWORK</h4>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <img src="images/pp.jpg" class="img-circle img-responsive" />

<p> Good Security service and reliable Security Guards.They are Providing 100% protection and high quality Security Service to every one.</p>                  
<h4>Client 2</h4>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <img src="images/old/fast logistics.jpg" class="img-circle img-responsive" />

<p>service was excellent...manpower is really quick and genuine...tanx for the service
</p>                            <h4>Ashok kumar, Commercial Head</h4>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </section>
     <div class="foot">
         <div class="container">
            <img src="images\FWG_web_Footer.png" style="padding-top: 05%;width:40%">
            <h2>Freightwatch G Security Services India Private Limited</h2>
         </div>
      </div>
      <div class="footer">
         Developed by <font color="#59FF00">Anlyz</font><font color="#0039BF">360</font>
      </div>
   </div>

      <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d5bcd5477aa790be32fce15/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
    
    
       <script>
    function myFunction1() {
      var x = document.getElementById("myDIV1");
      if (x.style.display === "none") {
        x.style.display = "block";
      } else {
        x.style.display = "none";
      }
    }
    </script>
            <script>
              window.addEventListener('scroll', scrollEvent)
                var hide =document.getElementById('hide')
                console.log(hide.classList)

                function scrollEvent(){
                  if(window.pageYOffset > 300)
                {
                  hide.classList.remove('hide') 
                } else if(window.pageYOffset < 300){
                  hide.classList.add('hide') 

                }
                }



                </script>
                 <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
                 <script>
                   AOS.init();
                 </script>
                <script>
                
$('.jquery-background-video').bgVideo({fadeIn: 2000});
                </script>
   </body>
</html>















<!--<html>
   <head>
      <title>Fwg </title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script src="java.js"></script>
      <link rel="stylesheet" href="ostyle.css">
      <style>
          #mySidenav a {
      position: fixed;
      left: -80px;
      transition: 0.3s;
      padding: 15px;
      width: 100px;
      text-decoration: none;
      font-size: 20px;
      margin-top:210px;
      color: white;
      border-radius: 0 5px 5px 0;
    }
    
    #mySidenav a:hover {
      left: 0;
    }
    
    #about1 {
      top: 20px;
      background-color: blue;
    }
    
    #blog {
      top: 80px;
      background-color:#f44336;
    }
    
    #projects {
      top: 140px;
      background-color: skyblue;
    }
    
    #contact1 {
      top: 200px;
      background-color: #555
    }
    </style>
       </head>
       <body>
             <div id="mySidenav" class="sidenav">
                   <a href="#" id="about1">FB</a>
                   <a href="#" id="blog">Call Me</a>
                   <a href="#" id="projects">Email</a>
                   <a href="#" id="contact1">Feedback</a>
                 </div>
                 
          <div class="cursor"></div>
          <div class="brandname">
          <div class="container">
            <h1>
      
             Freightwatch G Patomotoh
             Security Services
    
            </h1>
    <img src="hd logo.jpg" class="logoid">
            </div>
          </div>
          <section id="header">
        
             <div class="menu-bar">
            
             <nav class="navbar navbar-expand-lg navbar-light hide" id="hide"> 
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="container">
                <div class="collapse navbar-collapse" id="navbarNav">
                                   <ul class="navbar-nav">
                                  
                      <li class="nav-item active">
                         <a class="nav-link" href="#">Home</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="about.html">About Us</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="ourservice.html">Services</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="news.html">Why Us</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="news.html">News</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link link" href="contact.html">Contact</a>
                      </li>
                   </ul>
                </div></div></div>
             </nav>
             <section id="header1">
        
                <div class="menu-bar1">
               
                <nav class="navbar1 navbar-expand-lg navbar-light">
                   <a class="navbar-brand" href="#"></a>
                   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                   <span class="navbar-toggler-icon"></span>
                   </button>
                   <div class="container">
                   <div class="collapse navbar-collapse" id="navbarNav">
                                      <ul class="navbar-nav">
                                     
                         <li class="nav-item active">
                            <a class="nav-link" href="#">Home</a>
                         </li>
                         <li class="nav-item">
                            <a class="nav-link" href="about.html">About Us</a>
                         </li>
                         <li class="nav-item">
                            <a class="nav-link" href="ourservice.html">Services</a>
                         </li>
                         <li class="nav-item">
                            <a class="nav-link" href="news.html">Why Us</a>
                         </li>
                         <li class="nav-item">
                            <a class="nav-link" href="news.html">News</a>
                         </li>
                         <li class="nav-item">
                            <a class="nav-link link" href="contact.html">Contact</a>
                         </li>
                      </ul>
                   </div></div></div>
                </nav></section>
             <!----  <div class="header">
                <div class="web_header_right">
                            <ul>
                              <li><i class="fa fa-phone" aria-hidden="true"></i>Tel 03 5567 9955</li>
                              <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:kvboakarur@gmail.com">info@fwg.my</a></li>
                            </ul>
                          </div>
                         
                          <div class="web_header_left">
                            <ul>
                                <li><a href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-user" aria-hidden="true"></i> Log in</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#myModal3"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign up</a></li>
                              </ul>
                      
                          </div>
                        <div class="clearfix"> </div>
                        </div>  --
          </section>
         
         
     
          <div class="ban">
              <h1 style=" text-align:center;
    font-weight:900;
    font-size:46px;color:white;margin-top:-8%;">Our services</h>
              
          </div>
  <br><br>
 <!-----services--
 <div class="services">
   <h1 style="text-align: center;font-weight: 900;">Services</h1>
    <div class="co">
          <div class="row">
            <div class="col-sm-6 co1">

            </div>
            <div class="col-sm-6 co2">

              </div>
          </div></div>
<h4 style="text-align: center; margin-top: 1%;">We are providing the Following World Class Services to Our Customers.</h4>
 <br> <br> <br>
   <div class="container">
    <div class="row rows">
          <div class="col-sm-4">
            
              <img src="cash-division.jpg"class="img-service">
              <div class="container">
                <h3 style="font-weight: 900;">Cash Division</h3>
                <p class="para">In this division, our service are designed for all types of business entities, which,
                   includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                    both in the local and international market.
                   We also provide tailor made services to meet the requirements of our distinguished clients.--</p>
                   <button type="button" class="viewbtn">View</button>

              </div>
            </div>
            <div class="col-sm-4">
            
                <img src="cash-division.jpg"class="img-service">
                <div class="container">
                  <h3 style="font-weight: 900;">Security System Solution
                    </h3>
                  <p class="para">In this division, our service are designed for all types of business entities, which,
                     includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                      both in the local and international market.
                     We also provide tailor made services to meet the requirements of our distinguished clients.--</p>
                     <button type="button" class="viewbtn">View</button>

                </div>
              </div>
              <div class="col-sm-4">
            
                  <img src="cash-division.jpg"class="img-service">
                  <div class="container">
                    <h3 style="font-weight: 900;">Dog Security</h3>
                    <p class="para">In this division, our service are designed for all types of business entities, which,
                       includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                        both in the local and international market.
                       We also provide tailor made services to meet the requirements of our distinguished clients.--/p>
                       <button type="button" class="viewbtn">View</button>

                  </div>
                </div></div><br>
                <div class="row rows">
                    <div class="col-sm-4">
            
                        <img src="cash-division.jpg"class="img-services">
                        <div class="container">
                          <h3 style="font-weight: 900;">Safety Division

                            </h3>
                          <p class="para">In this division, our service are designed for all types of business entities, which,
                             includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                              both in the local and international market.
                             We also provide tailor made services to meet the requirements of our distinguished clients.--</p>
                             <button type="button" class="viewbtn">View</button>

                        </div>
                      </div>
                      <div class="col-sm-4">
            
                          <img src="cash-division.jpg"class="img-services">
                          <div class="container">
                            <h3 style="font-weight: 900;">Guarding Division
                              </h3>
                            <p class="para">In this division, our service are designed for all types of business entities, which,
                               includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                                both in the local and international market.
                               We also provide tailor made services to meet the requirements of our distinguished clients.--</p>
                               <button type="button" class="viewbtn">View</button>

                          </div>
                        </div>
                        <div class="col-sm-4">
            
                            <img src="cash-division.jpg"class="img-services">
                            <div class="container">
                              <h3 style="font-weight: 900;">Systems & Solutions
                                </h3>
                              <p class="para">In this division, our service are designed for all types of business entities, which,
                                 includes financial institutions, <!---commercial outlets, hypermarkets and other businesses
                                  both in the local and international market.
                                 We also provide tailor made services to meet the requirements of our distinguished clients.--</p>
                                 <button type="button" class="viewbtn">View</button>

                            </div>
                          </div>
                </div>
          </div>
   </div></div>
<div class="serv">
  <h2 style="text-align: center;font-weight:900;">Brands We Work</h2>
  <br>
  <div class="row row23">
  <div class="col-sm-3">
    <div class="card">
<img src="toyoto.png"class="card-img" >
</div></div>
<div class="col-sm-3">
    <div class="card">
        <img src="toyoto.png"class="card-img" >

</div></div>
<div class="col-sm-3">
    <div class="card">
        <img src="toyoto.png"class="card-img" >

</div></div>
<div class="col-sm-3">
    <div class="card">
        <img src="toyoto.png"class="card-img" >

</div>
  </div> </div></div>
  <div class="row row231">
      <div class="col-3">
        <div class="card">
    <img src="toyoto.png"class="card-img" >
    </div></div>
    <div class="col-3">
        <div class="card">
            <img src="toyoto.png"class="card-img" >
    
    </div></div>
    <div class="col-3">
        <div class="card">
            <img src="toyoto.png"class="card-img" >
    
    </div></div>
    <div class="col-3">
        <div class="card">
            <img src="toyoto.png"class="card-img" >
    
    </div>
      </div> </div></div>
  </div>
  <div class="testimonial3">
<!-- Photo Grid --

<div class="benifits">
  <div class="benifit-content">
      <br>
  <h1 style="color:white; text-align:center;">SERVICES BENIFITS</h1>
</div><br><br>
<div class="container">
<div class="row">
  
      <div class="col-sm-4 col-b">
<h4 style="text-align: center;font-weight:  bold;">Technical Security Surveys</h4>
<p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
        
         
        
    The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
    </p>
  </div>
  <div class="col-sm-4 col-b" style="color:transparent;">
      <h4 style="text-align: center;font-weight:  bold;" ></h4>
      <p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
              
               
              
          The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
          </p>
  </div>
  <div class="col-sm-4 col-b">
      <h4 style="text-align: center;font-weight:  bold;">CCTV Network system</h4>
      <p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
              
               
              
          The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
          </p>
  </div>
</div><br><br>
<div class="row">
  
    <div class="col-sm-4 col-b1">
<h4 style="text-align: center;font-weight:  bold;">Mobile Petrol Management</h4>
<p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
      
       
      
  The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
  </p>
</div>
<div class="col-sm-4 col-b" style="color:transparent;">
    <h4 style="text-align: center;font-weight:  bold;"> Technical Security Surveys</h4>
    <p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
            
             
            
        The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
        </p>
</div>
<div class="col-sm-4 col-b">
    <h4 style="text-align: center;font-weight:  bold;">Access Control System</h4>
    <p>FWG staffs were gathered today and host a Deepavali lunch in respect to all the Hindu colleagues celebrating the festival of lights. All the staffs came dressed in beautiful Indian traditional wear and decorated the office with colourful rangoli kolam and lights.
            
             
            
        The FWG management had arranged for sumptuous Deepavali lunch and snacks and ended with all the staffs receiving their Deepavali gift.
        </p>
</div>
</div>
</div></div>
<section class="testimonials text-center">
            <div class="container">
        
                <div class="heading white-heading">
                    Testimonial
                </div>
                <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
                 
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="testimonial4_slide">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" />
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                <h4>Client 1</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" /><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                <h4>Client 2</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" />
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                <h4>Client 3</h4>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
        </section>
         <div class="foot">
         <div class="container">
            <img src="images\FWG_web_Footer.png" style="padding-top: 05%;width:40%">
            <h2>Freightwatch G Security Services India Private Limited</h2>
         </div>
      </div>
      <div class="footer">
         Developed by <font color="#59FF00">Anlyz</font><font color="#0039BF">360</font>
      </div>
</body></html>

-->



