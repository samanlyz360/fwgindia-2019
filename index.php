<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>FreightWatch G Security Services India Private Limited  </title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
           

      <script src="https://npmcdn.com/flickity@2/dist/flickity.pkgd.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
       <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
     <link rel="stylesheet" href="css/style.css">
      
      <style>
       .pec100
  {
   position:absolute;   
    margin-top: -92px;
    margin-left: 139px;
    font-size: 62px;
  }
      #mySidenav a {
  position: fixed;
  left: -80px;
  transition: 0.3s;
  padding: 15px;
  width: 100px;
  text-decoration: none;
  font-size: 20px;
  margin-top:210px;
  color: white;
  border-radius: 0 5px 5px 0;
}

#mySidenav a:hover {
  left: 0;
}

#about1 {
  top: 20px;
  background-color: blue;
}

#blog {
  top: 80px;
  background-color:#f44336;
}

#projects {
  top: 140px;
  background-color: skyblue;
}

#contact1 {
  top: 200px;
  background-color: #555
}
.brandname {
    height: 127px;
}
.logoid {
    /* height: 10px; */
    display: inline;
    z-index: 1;
    height: 67px;
    z-index: 1;
    margin-left: 36px;
}
.brandname h1 {
    color: #8E343E;
    text-transform: initial;
    font-weight: bold;
    text-align: center;
    font-size: 37px;
        margin-top: -58px;
    width: 61%;
    /* margin-top: -115px; */
    margin-left: 260px;
}
.brandname h4
{
color: #8E343E;
    margin-left: 14px;
    /* font-size: 27px; */
    font-weight: bold;
}
@media screen and (max-width: 460px){
.brandname h1 {
    color: #8E343E;
    text-transform: initial;
    font-weight: bold;
    text-align: center;
    font-size: 31px;
    margin-top: -36px;
    text-shadow: none;
    font-family: 'Roboto Condensed', sans-serif;
    /* width: 61%; */
    /* margin-top: -115px; */
    margin-left: 5px;
}
}
</style>
   </head>
   <body>
         <div id="mySidenav" class="sidenav">
               <a href="https://www.facebook.com/fwgind/" id="about1" target="_blank">FB</a>
               <a href="callto:8861715281" id="blog" >Call Me</a>
               <a href="mailto:info@fwg.my" id="projects">Email</a>
               <a href="mailto:info@fwg.my" id="contact1">Feedback</a>
             </div>
             
      <div class="cursor"></div>
        <div class="brandname">
               <a href="index.php"> <img src="images/FWGIndialogo.jpg" class="logoid"></a>
         <h1>
            FreightWatch G Security Services India Pvt Ltd
                </h1>
                <h4>Securing Global Trade</h4>
                </div>
              <section id="header">
            
                 <div class="menu-bar">
                
                 <nav class="navbar navbar-expand-lg navbar-light hide" id="hide"> 
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" 
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNav">
                         <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php"style="color:black;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div>
                 </nav></section>
                 
                 <section id="header1">
            
                    <div class="menu-bar1">
                   
                    <nav class="navbar1 navbar-expand-lg navbar-light">
                         <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon navbar-toggler-icon1"></span>
                    </button>
                    <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNav">
                                          <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ourservice.php">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="why_us.php">Why_Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact_Us</a>
      </li>
     </ul>
     </div></div></div></nav>                      
      </section>
       <!-- Start video hero -->
       <div class="video-hero jquery-background-video-wrapper demo-video-wrapper">
         <video class="jquery-background-video" autoplay muted loop poster="">
           <source src="images/FWG_Corporate_Video.mp4" type="video/mp4">
         </video>
            <div class="video-overlay"></div>
            <div class="page-width">
                 <div class="video-hero--content">
                      <h1>Fwg India</h1>
                      <p>India is Safe Now</p>
                 </div>
            </div>
       </div>
       <!-- End video hero -->
       
       <div id="content"data-aos="fade-up">
            <div class="page-width">
                 <h2 style="font-weight:bold;">Who We Are</h2>
                 <p style="text-align: justify;font-size:18px;
    letter-spacing: 1px;">We are FWG, FreightWatch G Security Services have two decades of experience in Security Solutions,
    We help by delivering the right skills and expertise to manage the different complexities in the security technology.
    Our combined team has experience in Services including; Manned Security Services (MSS), Security Technology Solutions, Security Allied Consultancy Services,
    Security Training, Specialized Training, Risk Management, Central Monitoring Services..

We provide   effective, consistent and quality service to our clients through technology, commitment and service excellence aimed to achieve customer satisfaction.
We reduce the cost planning and implementation, Achieve outcomes with end-to-end management of our clients security, Free-up our clients to stay focused on business critical functions,
Maximise the value of security solutions so our clients achieve a better security posture, Implementation by trained and experienced guards with industry experience.
                     </p>
                 </div>
       </div>
   
      <div class="services"data-aos="fade-up"><h1>What We Do</h1>
         <div class="co">
               <div class="row">
                  <div class="col-sm-6 co1">
                  </div>
                  <div class="col-sm-6 co2">
                  </div>
               </div></div><br><br>
        <div class='services_items'>
           <div class="container">
               <div class="row">
                <div class="col-sm-6 image">
                   <div class="row">
                      <div class="col-sm-3">
                        <!-- <img src="fingerprint.png"  class="services1">-->
                      </div>
                     <div class="col-sm-9 best"data-aos="flip-up">
                        <h4>MANNED SECURITY SERVICES</h4></a>
                         <h5>We are Providing World class Manned Security Service to Our Customer.</h5>
                        <!--- <a href="ourservice.html/id1">View</a>--->
                      </div>
                   </div>
                </div>
                <div class="col-sm-6 image">
                   <div class="row">
                      <div class="col-sm-3">
                        <!--- <img src="fingerprint.png"   class="services1">--->
                      </div>
                      <div class="col-sm-9 best"data-aos="flip-up">
                         <h4>SECURITY TECHNOLOGY SERVICES</h4>
                         <h5> It is Fully Depend on Security Devices.Through this We can do 100% Scanning purpose to Customer Safety.                 </h5>
                        <!-- <a href="ourservice.html/id1">View</a>-->
                      </div>
                   </div>
                </div>
              
             </div>
             <div class="row">
                <div class="col-sm-6 image">
                   <div class="row">
                      <div class="col-sm-3">
                        <!--- <img src="fingerprint.png"  class="services1" >-->
                      </div>
                      <div class="col-sm-9 best"data-aos="flip-up">
                         <h4>SECURITY TRAINING</h4>
                         <h5>The Best Professional trainers we have to Providing the security Guard training                         </h5>
                         <!---<a href="ourservice.html/id1">View</a>--->
                      </div>
                   </div>
                </div>
                <div class="col-sm-6 image">
                   <div class="row">
                      <div class="col-sm-3">
                        <!--- <img src="fingerprint.png"  class="services1">-->
                      </div>
                      <div class="col-sm-9 best"data-aos="flip-up">
                         <h4>Security Allied Consultancy</h4>
                         <h5>Our Guards are care about your safety,So We would comform Your full Safety.
                         <!---</h5><a href="ourservice.html/id1">View</a>-->
                      </div>
                   </div>
                </div>
              
             </div>
        </div></div></div>

      </div>
      <div class="awards"data-aos="fade-up">
         <h1>Accreditations</h1>
         <div class="co">
            <div class="row">
               <div class="col-sm-6 co1">
               </div>
               <div class="col-sm-6 co2">
               </div>
            </div>
         </div>
         <div class="wrapper">
            <div class="photobanner">
                
                
               <img class="first" src="images\FWG_Certificate1.jpg" alt="FWG Best Enterprises Certificate" class="cos"/>" />
               <img class="first" src="images\FWG_Certificate2.jpg" alt="FWG International Quality Crown award"class="cos"/>
               <img class="first" src="images\FWG_Certificate3.jpg" alt="FWG International Quality Crown award"class="cos" />
               <img class="first" src="images\FWG_Certificate4.jpg" alt="FWG BS OHSAS 18001:2007" class="cos"/>
               <img class="first" src="images\FWG_Certificate5.jpg" alt="FWG Award" class="cos" />
               <img class="first" src="images\FWG_Certificate6.jpg" alt="FWG 9001:2015"class="cos" />
               <img class="first" src="images\FWG_Certificate7.jpg" alt="FWG European award for best practice"class="cos" />
               <img class="first" src="images\FWG_Certificate8.jpg" alt="FWG Award" class="cos" />
               <img class="first" src="images\license.jpg" alt="FWG TAPA Membership certificate" class="cos" />

            </div>
         </div>
      </div>
      <div class="counting">
         <h1 style=text-align:center;>Our Achievements</h1>
         <br>
         <div class="container container-coun" data->
            <div class="row">
               <div class="col-sm-3 col-coun">
                  <div id="box">
                     <h1>Happy<br/>Clients</h1>
                     <span class="clients counts"></span>
                  </div>
               </div>
               <div class="col-sm-3 col-coun">
                  <div id="box">
                     <h1>Branches <br/>We have</h1>
                     <span class="branch counts"></span>
                  </div>
               </div>
               <div class="col-sm-3 col-coun">
                  <div id="box">
                     <h1>awards<br/>Received</h1>
                     <span class="total counts"></span>
                  </div>
               </div>
               <div class="col-sm-3 col-coun">
                  <div id="box">
                     <h1>Service<br/>Quality</h1>
                     <span class="total counts"></span>
                     <span class="pec100">%</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script>
         //Animate my counter from 0 to set number (6)
         $({counter: 0}).animate({counter: 321}, {
           //Animate over a period of 2seconds
           duration: 15500,
           //Progress animation at constant pace using linear
           easing:'linear',
           step: function() {    
             //Every step of the animation, update the number value
             //Use ceil to round up to the nearest whole int
             $('.clients').text(Math.ceil(this.counter))
           },
           complete: function() {
             //Could add in some extra animations, like a bounc of colour change once the count up is complete.
           }
         });
          
          
          //Animate my counter from 0 to set number (6)
         $({counter: 0}).animate({counter: 26}, {
           //Animate over a period of 2seconds
           duration: 5000,
           //Progress animation at constant pace using linear
           easing:'linear',
           step: function() {    
             //Every step of the animation, update the number value
             //Use ceil to round up to the nearest whole int
             $('.branch').text(Math.ceil(this.counter))
           },
           complete: function() {
             //Could add in some extra animations, like a bounc of colour change once the count up is complete.
           }
         }); 
         
         
         //Animate my counter from 0 to set number (6)
         $({counter: 0}).animate({counter: 100}, {
           //Animate over a period of 2seconds
           duration: 9500,
           //Progress animation at constant pace using linear
           easing:'linear',
           step: function() {    
             //Every step of the animation, update the number value
             //Use ceil to round up to the nearest whole int
             $('.total').text(Math.ceil(this.counter))
           },
           complete: function() {
             //Could add in some extra animations, like a bounc of colour change once the count up is complete.
           }
         });
         

         
         
         
      </script>   
      
  <!--  <div class="video">
      
    <h1>Accreditations</h1>

<!-- Flickity HTML init --
<div class="carousel22"data-aos="zoom-in-out"
  data-flickity='{ "imagesLoaded": true, "percentPosition": true }'>
    
  <img src="images\FWG_Certificate1.jpg" alt="FWG Best Enterprises Certificate" class="cos"/>
  <img src="images\FWG_Certificate2.jpg" alt="FWG International Quality Crown award"class="cos" />
  <img src="images\FWG_Certificate3.jpg" c />
  <img src="images\FWG_Certificate4.jpg" alt="FWG BS OHSAS 18001:2007" class="cos" />
  <img src="images\FWG_Certificate5.jpg" alt="FWG Award" class="cos" />
  <img src="images\FWG_Certificate6.jpg" alt="FWG 9001:2015"class="cos" />
  <img src="images\FWG_Certificate7.jpg" alt="FWG European award for best practice"class="cos" />
  <img src="images\FWG_Certificate8.jpg" alt="FWG Award" class="cos"/>
  <img src="images\FWG_Certificate9.jpg" alt="FWG TAPA Membership certificate" class="cos"/>
</div>
    </div>-->
      <div class="foot">
         <div class="container">
            <img src="images\FWG_web_Footer.png" style="padding-top: 05%;width:40%">
            <h2>Freightwatch G Security Services India Private Limited</h2>
         </div>
      </div>
      <div class="footer">
         Developed by <font color="#59FF00">Anlyz</font><font color="#0039BF">360</font>
      </div>

    
      <script type="text/javascript">
         jQuery(document).ready(function($) {
         	$(".scroll").click(function(event){		
         		event.preventDefault();
         		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
         	});
         });
      </script>
            <script>
              window.addEventListener('scroll', scrollEvent)
                var hide =document.getElementById('hide')
                console.log(hide.classList)

                function scrollEvent(){
                  if(window.pageYOffset > 300)
                {
                  hide.classList.remove('hide') 
                } else if(window.pageYOffset < 300){
                  hide.classList.add('hide') 

                }
                }
                </script>
                 <script>
              window.addEventListener('scroll', scrollEvent)
                var counts =document.getElementById('hide')
                console.log(hide)

                function scrollEvent(){
                  if(window.pageYOffset > 500)
                {
                  hide.classList.add('counts') 
                } else if(window.pageYOffset < 1100){
                  hide.classList.add('counts') 

                }
                }



                </script>
                <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d5bcd5477aa790be32fce15/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

                <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
                 <script>
                   AOS.init();
                 </script>
                <script>
                
$('.jquery-background-video').bgVideo({fadeIn: 2000});
                </script>
               
                 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
   </body>
</html>